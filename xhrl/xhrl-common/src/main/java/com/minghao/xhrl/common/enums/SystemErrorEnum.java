package com.minghao.xhrl.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by kobe on 2017/6/6.
 */
public enum SystemErrorEnum {

    SUCCESS("成功", "000000"),
    FAIL("失败", "000001");

    private String msg;
    private String code;

    SystemErrorEnum(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }

    //通过编码获取描述
    public static String getMsg(String code) {
        if(StringUtils.isNotEmpty(code)){
            for (SystemErrorEnum systemErrorEnum : SystemErrorEnum.values()) {
                if (systemErrorEnum.getCode().equals(code)) {
                    return systemErrorEnum.getMsg();
                }
            }
        }
        return null;
    }

    //通过描述获取编码
    public static String getCode(String msg) {
        if(StringUtils.isNotEmpty(msg)){
            for (SystemErrorEnum systemErrorEnum : SystemErrorEnum.values()) {
                if (systemErrorEnum.getMsg().equals(msg)) {
                    return systemErrorEnum.getCode();
                }
            }
        }
        return null;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
