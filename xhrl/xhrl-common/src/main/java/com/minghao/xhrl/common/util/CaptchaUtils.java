/**
 * Copyright (C) 2015 .
 * @Date:2015年1月12日 下午6:48:49
 *
 * @Version V1.0
 */
package com.minghao.xhrl.common.util;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.security.SecureRandom;


/**
 * @ClassName:RandomValidateCode
 * @Date:2015年1月12日 下午6:48:49
 * @Description:(生成图片验证码)
 * 
 */
public class CaptchaUtils {
	public static final String IMAGE_RANDOM_CODEKEY = "RANDOMVALIDATECODEKEY";
	// 放到session中的key
	public static final String RANDOMCODEKEY = "RANDOMVALIDATECODEKEY";
	// 随机产生的字符串
	private static final String RANDOM_STR = "123456789ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz";
	// 图片宽
	private static final int width = 80;
	// 图片高
	private static final int height = 26;
	// 干扰线数量
	private static final int lineSize = 10;
	// 随机产生字符数量
	private static final int stringNum = 4;
	// 最大图片像素值
	private static final int MAX_COLOR_PIXEL = 255;
	// 字体大小
	private static final int FONT_SIZE = 18;
	//颜色随机数最大值
	private static final int MAX_RANDOM = 100;
	
	private static SecureRandom random = new SecureRandom();
	

	/**
	 * 
	 * @Title: getFont
	 * @Description: (获得字体)
	 * @return
	 */
	public Font getFont() {
		return new Font("Fixedsys", Font.CENTER_BASELINE, FONT_SIZE);
	}

	/**
	 * 
	 * @Title: getRandColor
	 * @Description: (获得颜色)
	 * @param randomPixe1
	 *            随机数字 生成图片颜色像素
	 * @param randomPixe2
	 *            随机数字 生成图片颜色像素
	 * @return
	 */
	private Color getRandColor(int randomPixe1, int randomPixe2) {
		int randomNum = random.nextInt(MAX_RANDOM);
		
		if (randomPixe1 > MAX_COLOR_PIXEL) {
			randomPixe1 = MAX_COLOR_PIXEL;
		}
		if (randomPixe2 > MAX_COLOR_PIXEL) {
			randomPixe2 = MAX_COLOR_PIXEL;
		}
		if (randomPixe1 <= randomPixe2) {
			randomPixe2 = randomPixe1 / 2;
		}
		
		int r = randomNum + random.nextInt(randomPixe1 - randomPixe2);
		int g = randomNum + random.nextInt(randomPixe1 - randomPixe2);
		int b = randomNum + random.nextInt(randomPixe1 - randomPixe2);
		return new Color(r, g, b);
	}

	/**
	 * 生成随机图片
	 */
	public void getRandcode(HttpServletResponse response) {
		// BufferedImage类是具有缓冲区的Image类,Image类是用于描述图像信息的类
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
		Graphics g = image.getGraphics();
		g.fillRect(0, 0, width, height);
		g.setFont(getFont());
		g.setColor(getRandColor(175, 110));
		// 绘制干扰线
		for (int i = 0; i <= lineSize; i++) {
			drowLine(g);
		}
		// 绘制随机字符
		String randomString = "";
		for (int i = 1; i <= stringNum; i++) {
			randomString = drowString(g, randomString, i);
		}
		
//		Session session = SecurityUtils.getSubject().getSession();
//		session.setAttribute(CaptchaUtils.IMAGE_RANDOM_CODEKEY, randomString.toUpperCase());
		
		g.dispose();
		try {
			// 将内存中的图片通过流动形式输出到客户端
			ImageIO.write(image, "JPEG", response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	/**
//	 *
//	 * @Title: verifyValidateCode
//	 * @Description: (验证验证码)
//	 * @param validateCode
//	 * @return
//	 */
//	public static boolean verifyValidateCode(String validateCode) {
//		Session session = SecurityUtils.getSubject().getSession();
//		String code = (String) session.getAttribute(CaptchaUtils.IMAGE_RANDOM_CODEKEY);
//
//		if (null == code || !code.equals(validateCode.toUpperCase())) {
//			return false;
//		}
//
//		return true;
//	}

	/*
	 * 绘制字符串
	 */
	private String drowString(Graphics g, String randomString, int i) {
		g.setFont(getFont());
		g.setColor(new Color(random.nextInt(101), random.nextInt(111), random.nextInt(121)));
		String rand = String.valueOf(getRandomString(random.nextInt(RANDOM_STR.length())));
		
		randomString += rand;
		g.translate(random.nextInt(3), random.nextInt(3));
		g.drawString(rand, 13 * i, FONT_SIZE);
		return randomString;
	}

	/*
	 * 绘制干扰线
	 */
	private void drowLine(Graphics g) {
		int x = random.nextInt(width);
		int y = random.nextInt(height);
		int xl = random.nextInt(13);
		int yl = random.nextInt(15);
		g.drawLine(x, y, x + xl, y + yl);
	}

	/*
	 * 获取随机的字符
	 */
	private String getRandomString(int num) {
		return String.valueOf(RANDOM_STR.charAt(num));
	}
}