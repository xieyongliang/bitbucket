package com.minghao.xhrl.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by kobe on 2017/6/6.
 */
public enum LoginErrorEnum {

    ACCOUNT_OR_PASSWORD_ERROR("账号或密码不正确", "00001"),
    ACCOUNT_LOCKED_ERROR("账号已被锁定", "00002");

    private String msg;
    private String code;

    LoginErrorEnum(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }

    //通过编码获取描述
    public static String getMsg(String code) {
        if(StringUtils.isNotEmpty(code)){
            for (LoginErrorEnum loginErrorEnum : LoginErrorEnum.values()) {
                if (loginErrorEnum.getCode().equals(code)) {
                    return loginErrorEnum.getMsg();
                }
            }
        }
        return null;
    }

    //通过描述获取编码
    public static String getCode(String msg) {
        if(StringUtils.isNotEmpty(msg)){
            for (LoginErrorEnum loginErrorEnum : LoginErrorEnum.values()) {
                if (loginErrorEnum.getMsg().equals(msg)) {
                    return loginErrorEnum.getCode();
                }
            }
        }
        return null;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
