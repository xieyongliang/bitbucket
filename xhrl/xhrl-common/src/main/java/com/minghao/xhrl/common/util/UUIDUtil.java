package com.minghao.xhrl.common.util;

import java.util.UUID;

public class UUIDUtil {
	/**
	 * 随机生成UUID 32位字符串
	 * @return String uuid
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
