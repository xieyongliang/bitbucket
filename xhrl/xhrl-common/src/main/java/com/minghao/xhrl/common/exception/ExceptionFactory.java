package com.minghao.xhrl.common.exception;

/**
 * Created by kobe on 2017/3/7.
 */
public class ExceptionFactory {

    public static RuntimeException wrapException(String message) {
        throw new AppException(message);
    }
}
