package com.minghao.xhrl.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kobe on 2017/6/27.
 */
public class DateUtils {

    public static final ThreadLocal<DateFormat> FORMAT_DATE = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    public static final ThreadLocal<DateFormat> FORMAT_DATE_TIME = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };

    /**
     * 获取当前日期字符串
     * @return String
     */
    public static String getCurrDate() {
        return FORMAT_DATE.get().format(new Date());
    }

    /**
     * 获取当前时间字符串
     * @return String
     */
    public static String getCurrDateTime() {
        return FORMAT_DATE_TIME.get().format(new Date());
    }
}
