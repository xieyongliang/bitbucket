package com.minghao.xhrl.common.exception;

/**
 * 校验运行时异常
 * Created by kobe on 2017/3/7.
 */
public class ValidateException extends RuntimeException{

    public ValidateException() {
        super();
    }

    public ValidateException(String msg) {
        super(msg);
    }

    public ValidateException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ValidateException(Throwable cause) {
        super(cause);
    }

}
