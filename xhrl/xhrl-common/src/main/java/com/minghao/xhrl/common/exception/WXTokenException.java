package com.minghao.xhrl.common.exception;

/**
 * Created by kobe on 2017/3/7.
 */
public class WXTokenException extends RuntimeException{

    public WXTokenException() {
        super();
    }

    public WXTokenException(String msg) {
        super(msg);
    }

    public WXTokenException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public WXTokenException(Throwable cause) {
        super(cause);
    }

}
