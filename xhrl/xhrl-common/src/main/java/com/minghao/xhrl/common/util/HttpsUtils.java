package com.minghao.xhrl.common.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import java.io.*;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

/**
 * <code>HttpsUtils</code>基于https请求的工具类
 */
public class HttpsUtils {
    private static final Logger logger = LoggerFactory.getLogger(HttpsUtils.class);
    private static final String DEFAULT_CHAR_SET = "UTF-8";
    private static final String USER_AGENT = "Mozilla/5.0";

    /**
     * 以get方式发送https请求，获取响应的JSON字符窜
     * @param url 请求的基于https的url
     * @return 返回url响应的JSON字符窜
     */
    public static String get(String url) {
        HttpsURLConnection https;

        try {
            https = (HttpsURLConnection) new URL(url).openConnection();
            if(null != https){
            	https.setRequestMethod(Method.GET.name());
            	https.setRequestProperty("User-Agent", USER_AGENT);
            	return read(https, DEFAULT_CHAR_SET);
            }
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage());
            }
        }

        return null;
    }

    /**
     * 通过post提交数据到指定的url，数据默认以UTF-8编码
     * @param url　请坟的基于https的url
     * @param message 提交的内容
     * @return String　返回url响应的message
     */
    public static String post(String url, String message) {
        HttpsURLConnection https;
        String msg = null;
        try {
            https = (HttpsURLConnection) new URL(url).openConnection();
            https.setSSLSocketFactory(createDefaultSSLSocketFactory());
            https.setDoInput(true);
            https.setDoOutput(true);

            https.setRequestMethod(Method.POST.name());

            write(https, message, null);

            msg = https.getResponseMessage();
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage());
            }
        }

        return msg;
    }

    /**
     * 创建一个默认的，空的信任管理工厂
     * @return 返回创建的信任管理工厂
     */
    private static SSLSocketFactory createDefaultSSLSocketFactory() {
        TrustManager[] tm = {new DefaultX509TrustManager()};
        SSLContext sslContext;
        SSLSocketFactory factory = null;
        try {
            sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tm, new SecureRandom());

            factory = sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | NoSuchProviderException | KeyManagementException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage());
            }
        }

        if(null == factory) {
            throw new RuntimeException("SSLSocketFactory is null");
        }

        return factory;
    }
    /**
     * 从流中读取数据
     * @param https
     * @param chaset
     * @return
     */
    private static String read(HttpsURLConnection https, String chaset) {
        chaset = StringUtils.isBlank(chaset) ? DEFAULT_CHAR_SET : chaset;
        BufferedReader in = null;
        StringBuilder msg = new StringBuilder();
        try {
            if(HttpsURLConnection.HTTP_OK == https.getResponseCode()) {
                in = new BufferedReader(new InputStreamReader(https.getInputStream(), chaset));
                String str;
                while ((str = in.readLine()) != null) {
                    msg.append(str);
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    if (logger.isErrorEnabled()) {
                        logger.error(e.getMessage());
                    }
                }
            }
        }
        return msg.toString();
    }

    /**
     * 写数据到对应的流
     * @param https HttpsURLConnection
     * @param msg 写出的消息
     * @param charset 写出消息的编码
     */
    private static void write(HttpsURLConnection https, String msg, String charset) {
        charset = (null == charset || "".equals(charset)) ? DEFAULT_CHAR_SET : charset;

        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(https.getOutputStream(), charset))) {
            out.write(msg);
            out.flush();
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage());
            }
        }
    }

    enum Method {
        GET, PUT, POST, DELETE
    }

}
