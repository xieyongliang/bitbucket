package com.minghao.xhrl.common.exception;

/**
 * Created by kobe on 2017/3/7.
 */
public class AppException extends RuntimeException{

    public AppException() {
        super();
    }

    public AppException(String msg) {
        super(msg);
    }

    public AppException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public AppException(Throwable cause) {
        super(cause);
    }

}
