package com.minghao.xhrl.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by kobe on 2017/3/7.
 */
public class Result {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private String msg;

    private String code;

    private boolean success = true;

    private Object data;

    public Result() {}

    public Result(String msg) {
        this.msg = msg;
    }

    public Result(boolean success) {
        this.success = success;
    }

    public Result(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }

    public Result(boolean success, String msg, String code) {
        this.success = success;
        this.msg = msg;
        this.code = code;
    }

    public Result(boolean success, String msg, String code, Object data) {
        this.success = success;
        this.msg = msg;
        this.code = code;
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public Result setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Result setCode(String code) {
        this.code = code;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public Result setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Result setData(Object data) {
        this.data = data;
        return this;
    }

    public Result set(Object enumeration) {
        return set(enumeration, "getCode", "getMsg");
    }

    public Result set(Object enumeration, String getCodeMethod, String getMsgMethod) {
        if(!(enumeration instanceof Enum)) {
            throw new IllegalArgumentException("the first argument must instanceof enum");
        }

        try {
            this.setCode(enumeration.getClass().getMethod(getCodeMethod).invoke(enumeration).toString());
            this.setMsg(enumeration.getClass().getMethod(getMsgMethod).invoke(enumeration).toString());
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            if(logger.isErrorEnabled()) {
                logger.error("error when setting Result's code or msg or success");
            }
        }
        return this;
    }
}
