package com.minghao.xhrl.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * 序列化，反序列化对象工具类
 */
public class SerializeUtils {
	private static final Logger logger = LoggerFactory.getLogger(SerializeUtils.class);

	/**
	 * 
	 *  serialize
	 * (获取对象序列化byte数组)
	 * @author Administrator
	 * @param object obj
	 * @return byte[]
	 */
	public static byte[] serialize(Object object) {
		byte[] bytes = null;

		if (null == object) {
			bytes = new byte[0];
		}

		if (!(object instanceof Serializable)) {
			throw new IllegalArgumentException(
					SerializeUtils.class.getSimpleName()
							+ " requires a Serializable object, but "
							+ object.getClass().getName()
							+ " not a Serializable object");
		}

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(os);
			oos.writeObject(object);
			oos.flush();
			bytes = os.toByteArray();
		} catch (IOException e) {
			if(logger.isErrorEnabled()) {
				logger.error("Failed to serialize", e);
			}
		}

		return bytes;
	}

	/**
	 * 
	 *  deserialize
	 * (将byte[]反序列化为对象)
	 * @param bytes byte
	 * @param <T> 实体类
	 * @return T
	 */
	public static <T>T deserialize(byte[] bytes) {
		T object = null;
		
		if(!isEmpty(bytes)) {
			ByteArrayInputStream is = new ByteArrayInputStream(bytes);
			try {
				ObjectInputStream ois = new ObjectInputStream(is);
				object = (T) ois.readObject();
			} catch (IOException e) {
				if(logger.isErrorEnabled()) {
					logger.error("Failed to deserialize", e);
				}
			} catch (ClassNotFoundException ex) {
				if(logger.isErrorEnabled()) {
					logger.error("Failed to deserialize object", ex);
				}
			}
		}
		
		return object;
	}
	
	private static boolean isEmpty(byte[] bytes) {
			return (null == bytes || bytes.length == 0);
	}
	
}
