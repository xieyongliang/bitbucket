package com.minghao.xhrl.core.filter.xssSql;

import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Created by wanglei on 2016/3/22.
 */
public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {
    public XssHttpServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String[] getParameterValues(String parameter) {
        String[] values = super.getParameterValues(parameter);

        if (null == values) {
            return null;
        }

        String[] encodedValues = new String[values.length];

        for (int i = 0; i < values.length; i++) {
            encodedValues[i] = cleanXSS(values[i]);
        }

        return encodedValues;
    }

    @Override
    public String getParameter(String parameter) {
        String value = super.getParameter(parameter);

        if (null == value) {
            return null;
        }

        return cleanXSS(value);
    }

    @Override
    public String getHeader(String header) {
        String value = super.getHeader(header);

        if (null == value) {
            return null;
        }

        return cleanXSS(value);
    }

    private String cleanXSS(String value) {
        return HtmlUtils.htmlEscape(value);
    }
}
