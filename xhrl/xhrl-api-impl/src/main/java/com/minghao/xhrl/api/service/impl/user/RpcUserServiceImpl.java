package com.minghao.xhrl.api.service.impl.user;

import com.minghao.xhrl.api.service.user.RpcUserService;
import com.minghao.xhrl.service.domain.user.User;
import com.minghao.xhrl.service.service.user.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by kobe on 2017/8/9.
 */
@Service(value = "rpcUserService")
public class RpcUserServiceImpl implements RpcUserService{

    @Resource(name = "userService")
    private UserService userService;

    @Override
    public User queryUser(User user) {
        return userService.queryUser(user);
    }
}
