package com.minghao.xhrl.redis;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.UUID;

public class RedisManager {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private ShardedJedisPool jedisPool;

	public ShardedJedisPool getJedisPool() {
		return jedisPool;
	}

	public void setJedisPool(ShardedJedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}
	
	/**
	 * String键值对存入redis
	 * @param key key值
	 * @param value 不可null或空字符
	 * @return String
	 */
    public String setString(String key, String value) {
		if(StringUtils.isEmpty(key) || StringUtils.isEmpty(value)) {
			return null;
		}
		ShardedJedis redis = getJedis();
		String result = redis.set(key, value);
		redis.close();
		return result;
	}

	/**
	 * String键值对存入redis
	 * @param key key值
	 * @param value 不可null或空字符
	 * @return String
	 */
	public String setString(String key, String value, int seconds) {
		if(StringUtils.isEmpty(key) || StringUtils.isEmpty(value)) {
			return null;
		}
		ShardedJedis redis = getJedis();
		String result = redis.set(key, value);
		if(0 != seconds) {
			redis.expire(key, seconds);
		}
		redis.close();
		return result;
	}
	
	/**
	 * 设置String String过去时间 在调用setString方法后调用此方法
	 * @param key key值
	 * @param seconds 时效
	 * @return Long
	 */
	public Long setExpire(String key, int seconds) {
		if(StringUtils.isEmpty(key)) {
			return null;
		}
		
		ShardedJedis redis = getJedis();
		
		Long result = redis.expire(key, seconds);
		redis.close();
		
		return result;
	}
	
	/**
	 * 通过key得到对应String value
	 * @param key key值
	 * @return String
	 */
	public String getString(String key) {
		if(StringUtils.isEmpty(key)) {
			return null;
		}

		ShardedJedis redis = getJedis();
		String result = redis.get(key);
		redis.close();
		return result;
	}
	
	/**
	 * 删除redis中对应key 存的 String value
	 * @param key key值
	 * @return Long
	 */
	public Long delString(String key) {
		if(StringUtils.isEmpty(key)) {
			return null;
		}
		
		ShardedJedis redis = getJedis();
	
		Long result = 0L;
		if(redis.exists(key)) {
			result = redis.del(key);	
		}
		redis.close();
		return result;
	}

	/**
	 * 获取分布式锁(简化版)
	 * @param lockKey 锁key
	 * @param expire 锁定时间
	 * @return boolean
	 */
	public boolean acquireDistributedLock(String lockKey, Integer expire) {
		if(StringUtils.isEmpty(lockKey)) {
			return false;
		}

		String distributedLockKey = "distributed_lock_key:" + lockKey;

		try (ShardedJedis redis = getJedis()) {
			if (redis.setnx(distributedLockKey, UUID.randomUUID().toString().replace("-", "")) == 1) {
				redis.expire(distributedLockKey, expire);
				if (logger.isInfoEnabled()) {
					logger.info("acquire distributed lockKey success, lockKey={}", distributedLockKey);
				}
				return true;
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("acquire distributed lockKey fail, lockKey={}", distributedLockKey);
			}
		}
		return false;
	}

    /**
     * 释放分布式锁
     * @param lockKey 锁key
     */
    public void releaseDistributedLock(String lockKey) {
        if(!StringUtils.isEmpty(lockKey)) {
            String distributedLockKey = "distributed_lock_key:" + lockKey;

            try (ShardedJedis redis = getJedis()) {
                redis.del(distributedLockKey);
            }
        }
    }

	/**
	 * 根据key 获取失效剩余时间
	 * @param key key
	 * @return Long
	 */
	public Long ttl(String key) {
		ShardedJedis redis = getJedis();
		Long ttl = redis.ttl(key);
		redis.close();
		return ttl;
	}
	
	/**
	 * 获取redis实例
	 * @return ShardedJedis
	 */
	private ShardedJedis getJedis() {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
		} catch(Throwable e) {
			if(logger.isErrorEnabled()) {
				logger.error("jedisPool.getResource() exception redis server must be start." + e);
			}
		}
		
		return jedis;
	}
}