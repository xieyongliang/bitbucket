package com.minghao.xhrl.redis;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.Properties;

/**
 * Created by kobe on 2017/6/6.
 */
public class RedisConfigurer extends PropertyPlaceholderConfigurer {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    //TODO 加解密盐值
    private static String salt="xhrl";

    /**
     * 数据库用户密码解密
     * @param beanFactory beanFactory
     * @param props props
     * @throws BeansException
     */
    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) throws BeansException {

        String passWordKey = "redis.password";
        String password = props.getProperty(passWordKey);
        try {
            if (StringUtils.isNotEmpty(password)) {
                //TODO 解密password
                props.setProperty(passWordKey, password);
            }
        } catch (Exception e) {
            logger.error("redis密码解密出现异常", e.getMessage());
        }
        super.processProperties(beanFactory, props);
    }
}
