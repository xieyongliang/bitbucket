package com.minghao.xhrl.redis;

import com.minghao.xhrl.common.util.Result;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * Created by kobe on 2017/3/8.
 */
public abstract class RedisLockUtils {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource(name = "redisManager")
    protected RedisManager redisManager;

    protected String locked = "locked:";

    protected String errorCount = "error_count:";

    /**
     * 默认最大失败次数(默认为5次)
     */
    protected Integer maxErrorCount = 5;

    /**
     * 默认锁定时间(单位:分钟) (默认30分钟)
     */
    protected Integer lockTime = 30;

    /**
     * 默认记录失败时间区间(单位:分钟) (默认10分钟)
     */
    protected Integer errorTimeRange = 10;

    public abstract void setLocked(String locked);

    public abstract void setErrorCount(String errorCount);
    /**
     * 判断是否被锁
     * @param checkKey 校验键值对
     * @return Boolean
     */
    public Boolean isLocked(String checkKey) {
        return !StringUtils.isBlank(redisManager.getString(locked + checkKey));
    }

    /**
     * 获取剩余被锁时间
     * @param lockedKey 锁定的键
     * @return Long 返回类型
     */
    public Long getRemainingLockTime(String lockedKey) {
        Long remainingLockTime = redisManager.ttl(locked + lockedKey);

        if(remainingLockTime == -1 || remainingLockTime == -2) {
            return 0L;
        }
        return remainingLockTime;
    }

    /**
     * 获取失败次数
     * @param errorKey 错误的键
     * @return Integer  返回类型
     */
    public Integer getErrorCount(String errorKey) {
        String loginErrorCount = redisManager.getString(errorCount + errorKey);

        if(StringUtils.isBlank(loginErrorCount)) {
            return 0;
        }
        return Integer.parseInt(loginErrorCount);
    }

    /**
     *设置失败次数
     * @param errorKey 错误的键
     * @param errorCount 错误统计数
     * @param duration 期间
     */
    public void setErrorCount(String errorKey, Integer errorCount, Integer duration) {
        redisManager.setString(this.errorCount + errorKey, String.valueOf(errorCount), duration);
    }

    /**
     * 清除失败次数
     * @param errorKey 错误的键
     */
    public void clearErrorCount(String errorKey) {
        redisManager.delString(errorCount + errorKey);
    }

    /**
     * 锁定
     * @param lockKey 锁定的键
     * @param lockTime  锁定时间
     */
    public void lock(String lockKey,Integer lockTime) {
        redisManager.setString(locked + lockKey, UUID.randomUUID().toString().replace("-", ""), lockTime);
        clearErrorCount(lockKey);
        try {
            this.afterLock(lockKey);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("error when after lock,lockKey:{},lockTime:{}", lockKey, lockTime);
            }
        }
    }

    /**
     * 解锁
     * @param lockedKey 锁定的键
     */
    public void release(String lockedKey) {
        redisManager.delString(locked + lockedKey);
        clearErrorCount(lockedKey);
    }

    /**
     * 检查是否被锁定了
     * @param checkKey 校验键值对
     * @return Result
     */
    public Result isLooked(String checkKey) {
        boolean isLocked = isLocked(checkKey);
        if(isLocked){
            //获取剩余锁定时间
            Long remainingLockTime = getRemainingLockTime(checkKey);
            remainingLockTime = ((remainingLockTime == 0L? 1L : remainingLockTime) / 60) + 1;
            return returnLockingInfo(remainingLockTime.intValue());
        }
        return new Result();
    }

    /**
     * 失败后尝试去锁定
     * @param checkKey 校验键值对
     * @return Result
     */
    public Result attemptToLock(String checkKey) {
        //获取失败次数
        Integer errorCount = getErrorCount(checkKey);
        //与最大可失败次数进行对比
        if((errorCount + 1) >= maxErrorCount) {
            //锁定
            lock(checkKey, lockTime * 60);
            return returnLockingInfo(lockTime);
        }

        //记录N分钟内的失败次数
        setErrorCount(checkKey, errorCount + 1, errorTimeRange * 60);
        return returnRemainingAttemptTimesInfo(maxErrorCount - (errorCount + 1));
    }

    /**
     * 返回锁定时的信息
     * @param remainingLockTime 剩余锁定时间
     * @return Result
     */
    protected abstract Result returnLockingInfo(Integer remainingLockTime);

    /**
     * 返回剩余可尝试次数的信息
     * @param remainingAttemptTimes 剩余可尝试次数
     * @return Result
     */
    protected abstract Result returnRemainingAttemptTimesInfo(Integer remainingAttemptTimes);

    /**
     * 锁住后的操作,留给子类去实现具体方法
     * @param lockKey 锁定的键
     */
    protected abstract void afterLock(String lockKey);
}
