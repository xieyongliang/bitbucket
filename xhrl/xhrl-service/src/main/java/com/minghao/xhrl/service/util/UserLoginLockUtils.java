package com.minghao.xhrl.service.util;

import com.minghao.xhrl.common.enums.LoginErrorEnum;
import com.minghao.xhrl.common.util.Result;
import com.minghao.xhrl.redis.RedisLockUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by kobe on 2017/6/26.
 */
@Component(value = "userLoginLockUtils")
public class UserLoginLockUtils extends RedisLockUtils {

    @Value(value = "locked_user_login:")
    @Override
    public void setLocked(String locked) {
        this.locked = locked;
    }

    @Value(value = "error_count_user_login:")
    @Override
    public void setErrorCount(String errorCount) {
        this.errorCount = errorCount;
    }

//    @Value(value = "5")
//    @Override
//    public void setMaxErrorCount(Integer maxErrorCount) {
//        this.maxErrorCount = maxErrorCount;
//    }
//
//    @Value(value = "30")
//    @Override
//    public void setLockTime(Integer lockTime) {
//        this.lockTime = lockTime;
//    }
//
//    @Value(value = "10")
//    @Override
//    public void setErrorTimeRange(Integer errorTimeRange) {
//        this.errorTimeRange = errorTimeRange;
//    }

    @Override
    protected Result returnLockingInfo(Integer remainingLockTime) {
        return new Result(false).set(LoginErrorEnum.ACCOUNT_LOCKED_ERROR).setMsg("您的账户已锁定,请于" + remainingLockTime + "分钟后再试");
    }

    @Override
    protected Result returnRemainingAttemptTimesInfo(Integer remainingAttemptTimes) {
        return new Result(false).set(LoginErrorEnum.ACCOUNT_OR_PASSWORD_ERROR);
    }

    @Override
    public void afterLock(String lockKey) {
        //TODO 踢出用户各个终端
    }
}
