package com.minghao.xhrl.service.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kobe on 2017/3/7.
 */
public class BaseEntity implements Serializable{
    private String id;

    private String version;

    private Date createdDate;

    private Integer currentPage = 1;

    private Integer pageSize = 10;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
