package com.minghao.xhrl.service.domain.validate.custom;

import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 自定义校验器,可为空字符串,不做正则验证;不为空字符串则验证正则
 */
public class CustomValidator implements ConstraintValidator<CustomRegexp, Object> {

	private CustomRegexp CustomRegexp;

	@Override
	public void initialize(final CustomRegexp constraintAnnotation) {
		this.CustomRegexp = constraintAnnotation;
	}

	@Override
	public boolean isValid(final Object value, ConstraintValidatorContext context) {
		if (value == null || "".equals(value)) {
			return true;
		} else if (StringUtils.isNotEmpty(CustomRegexp.regexp())) {
			Pattern pattern = Pattern.compile(CustomRegexp.regexp());
			Matcher matcher = pattern.matcher(value.toString().replaceAll("/", ""));
			return matcher.matches();
		} 
		return false;
	}

}
