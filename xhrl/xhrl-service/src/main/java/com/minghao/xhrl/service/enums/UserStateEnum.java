package com.minghao.xhrl.service.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by kobe on 2017/6/6.
 */
public enum UserStateEnum {
    ACTIVE("激活","1"),
    DEACTIVITE("未激活","0");

    String msg;
    String code;

    UserStateEnum(String msg,String code) {
        this.msg = msg;
        this.code = code;
    }

    //通过编码获取描述
    public static String getMsg(String code) {
        if(StringUtils.isNotEmpty(code)){
            for (UserStateEnum userStateEnum : UserStateEnum.values()) {
                if (userStateEnum.getCode().equals(code)) {
                    return userStateEnum.getMsg();
                }
            }
        }
        return null;
    }

    //通过描述获取编码
    public static String getCode(String msg) {
        if(StringUtils.isNotEmpty(msg)){
            for (UserStateEnum userStateEnum : UserStateEnum.values()) {
                if (userStateEnum.getMsg().equals(msg)) {
                    return userStateEnum.getCode();
                }
            }
        }
        return null;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
