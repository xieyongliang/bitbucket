package com.minghao.xhrl.service.service.user.imp;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.minghao.xhrl.service.domain.user.User;
import com.minghao.xhrl.service.service.user.UserService;
import com.minghao.xhrl.service.storage.user.UserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by kobe on 2016/9/6.
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Resource(name = "userMapper")
    private UserMapper userMapper;

    /**
     * 查找用户
     * @param user 用户
     * @return User
     */
    @Override
    public User queryUser(User user) {
        return userMapper.queryUser(user);
    }

    /**
     * 查找用户认证信息
     * @param userName 用户名
     * @return User
     */
    @Override
    public User queryUserAuthInfo(String userName) {
        return userMapper.queryUserAuthInfo(userName);
    }

    /**
     * 获取用户列表
     * @param user 用户
     * @return Page
     */
    @Override
    public Page<User> queryUserList(User user, boolean paging) {
        if(paging) {
            PageHelper.startPage(user.getCurrentPage(), user.getPageSize());
        }
        return userMapper.queryUserList(user);
    }
}
