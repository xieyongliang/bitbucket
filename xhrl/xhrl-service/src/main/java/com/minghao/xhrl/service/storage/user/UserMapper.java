package com.minghao.xhrl.service.storage.user;

import com.github.pagehelper.Page;
import com.minghao.xhrl.service.domain.user.User;
import org.springframework.stereotype.Repository;

/**
 * Created by kobe on 2016/9/6.
 */
@Repository("userMapper")
public interface UserMapper {

    /**
     * 查找用户
     * @param user 用户
     * @return User
     */
    User queryUser(User user);

    /**
     * 查找用户认证信息
     * @param userName 用户名
     * @return User
     */
    User queryUserAuthInfo(String userName);

    /**
     * 获取用户列表
     * @param user 用户
     * @return Page
     */
    Page<User> queryUserList(User user);
}
