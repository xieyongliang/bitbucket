package com.minghao.xhrl.service.domain.validate.custom;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 自定义验证，可为空字符串，不做正则验证;不为空字符串则验证正则
 */
@Constraint(validatedBy = {CustomValidator.class})
@Documented
@Target({ METHOD, FIELD, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface CustomRegexp {
	
	String message() default "validate failed";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	
	String regexp();
}
