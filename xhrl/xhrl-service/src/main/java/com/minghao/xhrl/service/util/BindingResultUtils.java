package com.minghao.xhrl.service.util;

import com.minghao.xhrl.common.exception.ValidateException;
import org.springframework.validation.BindingResult;

/**
 * 验证结果工具类
 */
public class BindingResultUtils {
    public static void validateResult(BindingResult result) {
        if(result.hasErrors()) {
            throw new ValidateException(result.getAllErrors().get(0).toString());
        }
    }
}
