package com.minghao.xhrl.service.domain.user.base;

import com.minghao.xhrl.service.domain.BaseEntity;

/**
 * Created by kobe on 2017/3/7.
 */
public class BaseUser extends BaseEntity {
    private String name;

    private String password;

    private String salt;

    private String phone;

    private Integer state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
