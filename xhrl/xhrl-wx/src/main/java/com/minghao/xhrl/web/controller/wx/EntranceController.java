package com.minghao.xhrl.web.controller.wx;

import com.minghao.xhrl.web.common.BaseController;
import com.minghao.xhrl.web.wxConfiguration.AccessToken;
import com.minghao.xhrl.web.wxConfiguration.Signature;
import com.minghao.xhrl.web.wxConfiguration.util.WXSignatureValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 与微信对接入口控制层
 */
@Controller
@RequestMapping(value = "/wx")
public class EntranceController extends BaseController{
    /**
     * 入口
     * @param signature 签名
     */
    @RequestMapping(value = "entrance", method = {RequestMethod.GET, RequestMethod.POST})
    public void entrance(Signature signature) {
        //设置跨域
        response.addHeader("Access-Control-Allow-Origin", "*");

        if(WXSignatureValidator.isSignatureRequest(request)) {
            signature(signature, response);
        }
        else {
//            try {
//                acceptMessage(request, response);
//            } catch (IOException e) {
//                logger.error(e.getMessage());
//            }
        }
    }

    /**
     * 验证微信签名
     * @param signature 签名
     * @param response 返回
     */
    private void signature(Signature signature, HttpServletResponse response) {
        //验证签名请求
        String result = WXSignatureValidator.ValidateSignature(signature, AccessToken.SIGNATURE_TOKEN);
        writeMessage(response, result);
    }

    /**
     * 将数据写回到流中返回给微信
     * @param response 返回
     * @param echoStr 回应内容
     */
    private void writeMessage(HttpServletResponse response, String echoStr) {
        try (PrintWriter out = response.getWriter()) {
            out.print(echoStr);
            out.flush();
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage());
            }
        }
    }
}
