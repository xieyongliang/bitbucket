package com.minghao.xhrl.web.wxConfiguration.util;

import com.minghao.xhrl.web.wxConfiguration.Signature;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * 微信验签工具类
 */
public class WXSignatureValidator {

    private static final Logger logger = LoggerFactory.getLogger(WXSignatureValidator.class);

    /**
     * 通过request method判断是否是验证签名
     * 如果是get方法则返回值为true,说明是验证签名的
     * 如果是post方法则返回值为false,说明是基于消息的
     *
     * @param request 请求
     * @return 返回是否是验证签名的
     */
    public static boolean isSignatureRequest(HttpServletRequest request) {
        if ("GET".equals(request.getMethod())) {
            return true;
        }
        return false;
    }

    /**
     * 执行验证签名需要做的工作
     *
     * @param signature 签名
     * @param token 钥匙
     * @return String 返回验证的结果，用于返回给response
     */
    public static String ValidateSignature(Signature signature, String token) {
        String result = "";
        signature.setToken(token);

        if (check(signature)) {
            if (logger.isInfoEnabled()) {
                logger.info("验签成功");
            }
            result = signature.getEchoStr();
        }

        return result;
    }

    /**
     * 验证微信签名对象是否通过
     * @param signature 签名对象
     * @return 返回签名是否通过
     */
    public static boolean check(Signature signature) {
        String[] sa = {signature.getToken(), signature.getTimestamp(), signature.getNonce()};
        Arrays.sort(sa);
        String sortStr = sa[0] + sa[1] + sa[2];
        String sha1 = DigestUtils.sha1Hex(sortStr);
        return sha1.equals(signature.getSignature());
    }

}
