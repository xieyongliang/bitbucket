package com.minghao.xhrl.web.exceptionhandler;

import com.alibaba.fastjson.JSONObject;
import com.minghao.xhrl.common.exception.AppException;
import com.minghao.xhrl.common.util.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by kobe on 2017/3/7.
 */
@Component
public class ExceptionHandlerResolver implements HandlerExceptionResolver {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response, Object o, Exception e) {

        logger.error(e.getMessage());

        String msg = "系统异常,请稍后再试";

        boolean isAjax = isAjax(request);
        if(isAjax) {
            if(e instanceof AppException) {
                msg = e.getMessage();
            }

            addPromptMessageWithAjaxException(response,msg);
            return null;
        }

        return new ModelAndView("error/500");
    }

    /**
     * 判断请求是否为ajax请求
     * @param request 请求
     * @return boolean
     */
    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    private void addPromptMessageWithAjaxException(HttpServletResponse response,String message) {
        Result result = new Result(false,message,null);
        response.setContentType("application/json;charset=utf-8");

        Writer writer = null;
        try {
            writer = response.getWriter();
            writer.write(JSONObject.toJSONString(result));

            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
