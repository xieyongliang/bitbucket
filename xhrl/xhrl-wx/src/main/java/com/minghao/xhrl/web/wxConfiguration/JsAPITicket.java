package com.minghao.xhrl.web.wxConfiguration;

import com.alibaba.fastjson.JSONObject;
import com.minghao.xhrl.common.util.HttpsUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

/**
 * 微信 jsapiTicket
 * Created by kobe on 2016/9/30.
 */
public class JsAPITicket {

    private static final Logger logger = LoggerFactory.getLogger(JsAPITicket.class);

    private static String JSAPI_TICKET;

    private static Date CREATED_DATE;

    /**
     * 微信JSAPITicket有效时间为2小时(120分钟),这里110分钟重新获取一次
     */
    private final static long EXPIRE_TIME = 1000 * 60 * 110;

    public static Signature getSignature(String url) {
        String nonceStr = UUID.randomUUID().toString().replaceAll("-", "");
        String timestamp = String.valueOf(System.currentTimeMillis() / 1000);

        Arrays.sort(nonceStr.getBytes());
        Arrays.sort(timestamp.getBytes());
        Arrays.sort(url.getBytes());

        String jsAPITicket = getJsAPITicket();
        Arrays.sort(jsAPITicket.getBytes());

        String sortStr = "jsapi_ticket="+ jsAPITicket +"&noncestr="+ nonceStr +"&timestamp="+ timestamp +"&url="+ url;
        String signatureStr = DigestUtils.sha1Hex(sortStr);

        Signature signature= new Signature();
        signature.setAppId(AccessToken.APPID);
        signature.setNonce(nonceStr);
        signature.setTimestamp(timestamp);
        signature.setSignature(signatureStr);
        return signature;
    }

    private static String getJsAPITicket() {
        if (null == JSAPI_TICKET || null == CREATED_DATE || new Date().getTime() - CREATED_DATE.getTime() >= EXPIRE_TIME) {
            synchronized (JsAPITicket.class) {
                //重新获取jsAPITicket
                String getTicketUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="
                        + AccessToken.getAccessToken()+"&type=jsapi";
                String resultJsonString = HttpsUtils.get(getTicketUrl);

                String jsAPITicket = JSONObject.parseObject(resultJsonString).getString("ticket");
                if (StringUtils.isBlank(jsAPITicket)) {
                    if (logger.isErrorEnabled()) {
                        logger.error("获取wx jsapi_ticket失败");
                    }
                    return "";
                }
                setJsapiTicket(jsAPITicket, new Date());
            }
        }
        return JSAPI_TICKET;
    }

    private static void setJsapiTicket(String jsapiTicket, Date createdDate) {
        JSAPI_TICKET = jsapiTicket;
        CREATED_DATE = createdDate;
    }
}
