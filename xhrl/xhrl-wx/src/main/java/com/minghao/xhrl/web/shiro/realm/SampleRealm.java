package com.minghao.xhrl.web.shiro.realm;

import com.minghao.xhrl.common.enums.LoginErrorEnum;
import com.minghao.xhrl.service.domain.user.User;
import com.minghao.xhrl.service.service.user.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;

/**
 * 用户认证与分配权限
 * Created by kobe on 2017/6/15.
 */
public class SampleRealm extends AuthorizingRealm {

    @Resource(name = "userService")
    public UserService userService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //TODO
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        Object principal = authenticationToken.getPrincipal();

        if(null == principal) {
            throw new AccountException(LoginErrorEnum.ACCOUNT_OR_PASSWORD_ERROR.getMsg());
        }

        String userName = principal.toString();

        User user = userService.queryUserAuthInfo(userName);

        if(null == user) {
            throw new AccountException("用户:" + userName + "," + LoginErrorEnum.ACCOUNT_OR_PASSWORD_ERROR.getMsg());
        }

        return new SimpleAuthenticationInfo(user.getId(), user.getPassword(), getName());
    }
}
