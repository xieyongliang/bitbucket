package com.minghao.xhrl.web.controller.login;

import com.minghao.xhrl.common.enums.LoginErrorEnum;
import com.minghao.xhrl.common.util.Result;
import com.minghao.xhrl.redis.RedisManager;
import com.minghao.xhrl.service.domain.user.User;
import com.minghao.xhrl.service.service.user.UserService;
import com.minghao.xhrl.service.util.BindingResultUtils;
import com.minghao.xhrl.service.util.UserLoginLockUtils;
import com.minghao.xhrl.web.common.BaseController;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Created by kobe on 2016/9/4.
 *
 * 登录
 */
@Controller
@RequestMapping(value = "/")
public class LoginController extends BaseController{
    @Resource(name = "userService")
    public UserService userService;

    @Resource(name = "redisManager")
    public RedisManager redisManager;

    @Resource(name = "userLoginLockUtils")
    public UserLoginLockUtils userLoginLockUtils;

    @RequestMapping
    public String toIndex() {
        return "index/index";
    }

    @RequestMapping(value = "toLogin")
    public ModelAndView toLogin() {
        ModelAndView mav = new ModelAndView("index/login");
        return mav;
    }

//    @RequestMapping(value = "doLogin", method = RequestMethod.POST)
    @PostMapping(value = "doLogin")
    @ResponseBody
    public Result doLogin(@Validated User user, BindingResult bindingResult) {
        //判断参数是否有误
        BindingResultUtils.validateResult(bindingResult);

        Result result = new Result();

        String userName = user.getName();
        String password = user.getPassword();

        Subject subject = SecurityUtils.getSubject();
        subject.logout();

        User info = userService.queryUserAuthInfo(userName);
        if(null == info) {
            return result.setSuccess(false).set(LoginErrorEnum.ACCOUNT_OR_PASSWORD_ERROR);
        }

        //判断是否已锁定
        result = userLoginLockUtils.isLooked(info.getId());
        if (!result.isSuccess()) {
            return result;
        }

        //解析password TODO

        //尝试登陆
        UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
        try {
            subject.login(token);
        } catch (AuthenticationException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage());
            }

            userLoginLockUtils.attemptToLock(user.getId());
            return result.setSuccess(false).set(LoginErrorEnum.ACCOUNT_OR_PASSWORD_ERROR);
        }

        //添加登陆日志 TODO

        return result;
    }
}
