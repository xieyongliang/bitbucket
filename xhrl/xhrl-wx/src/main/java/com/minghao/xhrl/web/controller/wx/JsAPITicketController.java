package com.minghao.xhrl.web.controller.wx;

import com.minghao.xhrl.common.exception.ExceptionFactory;
import com.minghao.xhrl.common.util.Result;
import com.minghao.xhrl.web.wxConfiguration.JsAPITicket;
import com.minghao.xhrl.web.wxConfiguration.Signature;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 获取jsAPI
 * Created by kobe on 2016/9/30.
 */
@Controller
@RequestMapping(value = "/wx")
public class JsAPITicketController {

    @PostMapping(value = "getJsAPISignature")
    @ResponseBody
    public Result getJsAPISignature(String url) {
        if(StringUtils.isBlank(url)) {
            throw ExceptionFactory.wrapException("error when getting jsAPITicket");
        }
        Signature signature = JsAPITicket.getSignature(url);
        return new Result().setData(signature);
    }

}
