package com.minghao.xhrl.web.wxConfiguration;

import com.alibaba.fastjson.JSONObject;
import com.minghao.xhrl.common.util.HttpsUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

/**
 * 微信 AccessToken
 * Created by kobe on 2016/8/19.
 */
public class AccessToken {

    private static final Logger logger = LoggerFactory.getLogger(AccessToken.class);

    static {
        //读取wechat配置文件并赋予APPID,SECRET
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("properties/wechat.properties");
            //TODO 解密APPID与SECRET
            APPID = properties.getProperty("appId");
            SECRET = properties.getProperty("secret");
            SIGNATURE_TOKEN = properties.getProperty("signature_token");
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage());
            }
        }
    }

    private static String ACCESS_TOKEN;

    private static Date CREATED_DATE;

    public static String APPID;

    private static String SECRET;

    public static String SIGNATURE_TOKEN;

    /**
     * 微信token有效时间为2小时(120分钟),这里110分钟重新获取一次
     */
    private final static long EXPIRE_TIME = 1000 * 60 * 110;

    /**
     * 获取access_token
     * @return String
     */
    public static String getAccessToken() {
        if (null == ACCESS_TOKEN || null == CREATED_DATE || new Date().getTime() - CREATED_DATE.getTime() >= EXPIRE_TIME) {
            synchronized (AccessToken.class) {
                //重新获取Token
                final String refreshTokenURL = "https://api.weixin.qq.com/cgi-bin/token?" +
                        "grant_type=client_credential&appid="+ APPID +"&secret="+ SECRET;
                String result = HttpsUtils.get(refreshTokenURL);

                String accessTokenObj = JSONObject.parseObject(result).getString("access_token");
                if (StringUtils.isBlank(accessTokenObj)) {
                    if (logger.isErrorEnabled()) {
                        logger.error("获取wx access_token失败");
                    }
                    return "";
                }
                setAccessToken(accessTokenObj, new Date());
            }
        }
        return ACCESS_TOKEN;
    }

    private static void setAccessToken(String accessToken, Date createdDate) {
        ACCESS_TOKEN = accessToken;
        CREATED_DATE = createdDate;
    }
}
