package com.minghao.xhrl.web.controller.login;

import com.github.pagehelper.Page;
import com.minghao.xhrl.service.domain.user.User;
import com.minghao.xhrl.service.service.user.UserService;
import com.minghao.xhrl.web.common.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by kobe on 2017/8/10.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController extends BaseController{

    @Resource(name = "userService")
    private UserService userService;

    @PostMapping(value = "queryUserList")
    public Page<User> queryUserList(User user) {
        return userService.queryUserList(user, true);
    }
}
