package com.minghao.xhrl.api.service.user;

import com.minghao.xhrl.service.domain.user.User;

/**
 * Created by kobe on 2017/8/9.
 */
public interface RpcUserService {

    /**
     * 查找用户
     * @param user 用户
     * @return User
     */
    User queryUser(User user);
}
